<?php

namespace Kanboard\Plugin\CASAuth\User;

use Kanboard\Core\User\UserProviderInterface;
use Kanboard\Model\LanguageModel;

/**
 * CAS User Provider
 *
 * @package  user
 */
class CasUserProvider implements UserProviderInterface
{
    protected $username;
    protected $name;
    protected $email;
    protected $role;
    protected $internalId;


    /**
     * Constructor
     *
     * @access public
     * @param  string   $username
     * @param  string   $name
     * @param  string   $email
     * @param  string   $role
     */
    public function __construct($username, $name, $email, $role, $internalId)
    {
        $this->username = $username;
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
        $this->internalId = $internalId;
    }

    /**
     * Return true to allow automatic user creation
     *
     * @access public
     * @return boolean
     */
    public function isUserCreationAllowed()
    {
        return true;
    }

    public function getInternalId()
    {
        return $this->internalId;
    }

    public function getExternalIdColumn()
    {
        return 'username';
    }

    public function getExternalId()
    {
        return $this->getUsername();
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getUsername()
    {
        return strtolower($this->username);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getExternalGroupIds()
    {
        return [];
    }

    public function getExtraAttributes()
    {
        $attributes = array('disable_login_form' => 1);
        return $attributes;
    }
}
