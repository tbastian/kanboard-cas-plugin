<?php

namespace Kanboard\Plugin\CASAuth\Auth;

require_once "CAS.php";

use Kanboard\Core\Base;
use Kanboard\Core\Security\Role;
use Kanboard\Core\Security\PreAuthenticationProviderInterface;
use Kanboard\Model\UserModel;
use Kanboard\Plugin\CASAuth\User\CasUserProvider;

/**
 * CAS Authentication Provider
 *
 * @package  auth
 */
class CasAuthProvider extends Base implements PreAuthenticationProviderInterface
{
    /**
     * User properties
     *
     * @access private
     * @var \Kanboard\Plugin\CASAuth\User\CasUserProvider
     */
    private $userInfo = null;

    /**
     * phpCas instance
     *
     * @access protected
     * @var \phpCas
     */
    protected $service;

    /**
     * Get authentication provider name
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'CAS';
    }

    /**
     * Authenticate the user
     *
     * @access public
     * @return boolean
     */
    public function authenticate()
    {
        try {
            $this->getService();
            $this->service->forceAuthentication();
            if ($this->service->checkAuthentication()) {
                $name = "";
                $email = "";
                $role = Role::APP_USER;

                if($this->service->hasAttribute('cn'))
                    $name = $this->service->getAttribute('cn');
                if($this->service->hasAttribute('email'))
                    $email = $this->service->getAttribute('email');
                elseif($this->service->hasAttribute('mail'))
                    $email = $this->service->getAttribute('mail');
                if(defined('CAS_ROLE_MANAGER') && CAS_ROLE_MANAGER)
                    $role = Role::APP_MANAGER;

                $this->userInfo = new CasUserProvider(
                    $this->service->getUser(),
                    $name,
                    $email,
                    $role,
                    $this->getInternalId()
                );
                return true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get the authenticated user's internal ID, if the user alrady exists;
     * otherwise return null
     */
    protected function getInternalId() {
        $user = $this->db
            ->table(UserModel::TABLE)
            ->columns('id')
            ->eq('username', $this->service->getUser())
            ->eq('disable_login_form', 1)
            ->eq('is_active', 1)
            ->findOne();
        if(! empty($user))
            return $user['id'];
        return null;
    }

    /**
     * Get user object
     *
     * @access public
     * @return CasUserProvider
     */
    public function getUser()
    {
        return $this->userInfo;
    }

    /**
     * Get CAS service
     *
     * @access public
     */
    public function getService()
    {
        //die();
        if (empty($this->service)) {
            $this->service = new \phpCAS();
            if(defined("CAS_DEBUG_LOG"))
                $this->service->setDebug(CAS_DEBUG_LOG);
            $this->service->client(CAS_VERSION_3_0, CAS_HOSTNAME, CAS_PORT, CAS_URI);
            $this->service->setCasServerCACert(CAS_CA_CERT);
        }
    }

    /**
     * logout from CAS
     *
     * @access public
     */
    public function logout()
    {
        $this->getService();

        if ($this->service) {
            $this->service->logout();
        }
    }
}
